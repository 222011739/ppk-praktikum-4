> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 4

[Index](/images/index.png "index")
1. GET
        >![GET](/images/GET.png "GET")
1. Get by ID
        >![GETbyID](/images/GetbyID.png "Get By ID")
1. POST
        >![POST](/images/POST.png "POST")
1. PUT
        >![PUT](/images/PUT.png "PUT")
1. DELETE
        >![DELETE](/images/DELETE.png "DELETE")

