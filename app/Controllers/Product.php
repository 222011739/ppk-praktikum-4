<?php

namespace App\Models;

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ProductModel;

/**
 * @OA\Info(title="PRODUCT M NAUFAL FAISHAL 3SI3", version="0.1")
 */


class Product extends ResourceController
{
    use ResponseTrait;

    // all users

    /**
     * @OA\Get(
     *     path="/product",
     *     summary = "Get All Product",
     *     tags = {"GET"},
     *     @OA\Response(response="200", description="Data berhasil ditampilkan")
     * )
     */

    public function index()
    {
        $model = new ProductModel();
        $data['produk'] = $model->orderBy('id', 'DESC')->findAll();

        return $this->respond($data);
    }

    // create

    /**
     * @OA\POST(
     *     path="/product",
     *     summary = "Add a Product",
     *          @OA\RequestBody(
     *              @OA\MediaType(
     *                  mediaType = "application/x-www-form-urlencoded",
     *                  @OA\Schema(
     *                      @OA\Property(
     *                          property = "nama_produk",
     *                          type = "string"
     *                      ),
     *                      @OA\Property(
     *                          property = "harga",
     *                          type = "integer"
     *                      )
     *                  )
     *              )         
     *      ),
     *     tags = {"POST"},
     *     @OA\Response(response="201", description="Data produk berhasil ditambahkan")
     * )
     */
    public function create()
    {
        $model = new ProductModel();
        $data = [
            'nama_produk' => $this->request->getVar('nama_produk'),
            'harga' => $this->request->getVar('harga'),
        ];

        $model->insert($data);
        $response = [
            'status' => 201,
            'error' => null,
            'messages' => [
                'success' => 'Data produk berhasil ditambahkan.'
            ]
        ];

        return $this->respondCreated($response);
    }

    // single user

    /**
     * @OA\Get(
     *     path="/product/{id}",
     *     summary = "Get product by id",
     *     tags = {"GET"},
     *          @OA\Parameter(
     *              name = "id",
     *              in = "path",
     *              required = true,
     *              description = "The id will be passed to {Id} to get the certain product" 
     *          ),
     *     @OA\Response(response="200", description="Data produk berhasil ditampilkan"),
     *     @OA\Response(response="404", description="Data produk tidak ditemukan"), 
     * )
     */

    public function show($id = null)
    {
        $model = new ProductModel();
        $data = $model->where('id', $id)->first();

        if ($data) {
            return $this->respond($data);
        } else {
            return $this->failNotFound('Data tidak ditemukan.');
        }
    }

    // update

    /**
     * @OA\Put(
     *     path="/product/{Id}",
     *     summary = "Update a Product by Id",
     *          @OA\Parameter(
     *              name = "id",
     *              in = "path",
     *              required = true,
     *              description = "The id will be passed to {Id} to select the certain product" 
     *          ),
     *          @OA\RequestBody(
     *              @OA\MediaType(
     *                  mediaType = "application/x-www-form-urlencoded",
     *                  @OA\Schema(
     *                      @OA\Property(
     *                          property = "nama_produk",
     *                          type = "string"
     *                      ),
     *                      @OA\Property(
     *                          property = "harga",
     *                          type = "integer"
     *                      )
     *                  )
     *              )         
     *      ),
     *     tags = {"Put"},
     *     @OA\Response(response="200", description="Data produk berhasil diupdate")
     * )
     */

    public function update($id = null)
    {
        $model = new ProductModel();
        // $id = $this->request->getVar('id');
        $data = [
            'nama_produk' => $this->request->getRawInput()['nama_produk'],
            'harga'  => $this->request->getRawInput()['harga'],
        ];
        $model->update($id, $data);
        $response = [
            'status'   => 200,
            'error'    => null,
            'messages' => [
                'success' => 'Data produk berhasil diubah.'
            ]
        ];
        return $this->respond($response);
    }

    // delete

    /**
     * @OA\Delete(
     *     path="/product/{id}",
     *     summary = "Delete a Product by Id",
     *          @OA\Parameter(
     *              name = "id",
     *              in = "path",
     *              required = true,
     *              description = "The id will be passed to {Id} to delete the certain product" 
     *          ),
     *     tags = {"Delete"},
     *     @OA\Response(response="200", description="Data produk berhasil dihapus")
     * )
     */

    public function delete($id = null)
    {
        $model = new ProductModel();
        $data = $model->where('id', $id)->delete($id);

        if ($data) {
            $model->delete($id);
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => [
                    'success' => 'Data produk berhasil dihapus.'
                ]
            ];
            return $this->respondDeleted($response);
        } else {
            return $this->failNotFound('Data tidak ditemukan.');
        }
    }

    public function APIDocumentation(){
        require("../vendor/autoload.php");

        $openapi = \OpenApi\Generator::scan(['../app/Controllers/Product.php']);

        header('Content-Type: application/json');
        return $this->respond(json_decode($openapi->toJSON()));
    }
}
